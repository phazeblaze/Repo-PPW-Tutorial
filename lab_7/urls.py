from django.conf.urls import url
from .views import index, validate_npm, delete_friend, friend_list, add_friend, get_friend_list, get_list_at_page

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-friend/$', add_friend, name='add-friend'),
    url(r'^validate-npm/$', validate_npm, name='validate-npm'),
    url(r'^delete-friend/$', delete_friend, name='delete-friend'),
    url(r'^friend-list/$', friend_list, name='friend-list'),
    url(r'^get-friend-list/$', get_friend_list, name='get-friend-list'),
    url(r'^change-page/$', get_list_at_page, name='change-page'),
]
